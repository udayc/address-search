<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>USER REGISTRATION</title>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
        <link href="{{ asset('autocomplete.css') }}" rel="stylesheet">  
        <style>
            #suggesstion-box span{
                color:#999999;
                display: flex; 
                margin:2px;               
            }
            #suggesstion-box{                
                background-color: #ffffff;
                border: 1px solid #ccc;
                padding: 5px;
                margin-bottom: 5px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>USER REGISTRATION</h2>                    
                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach 
                        </div>
                    @endif 
                    @if (Session::has('message'))
                        <div class="alert alert-success" role="alert"> 
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger" role="alert"> 
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    <form id="contact-form" method="POST" class="contact-form" action="user/store" enctype="multipart/form-data">
                        @csrf @method('POST')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input required="" type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') }}" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ old('email') }}" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone" value="{{ old('phone') }}" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input autocomplete="off" type="text" name="street_address" class="form-control" id="street_address" placeholder="Enter street address" value="{{ old('street_address') }}" > 

                                </div>
                                <div id="suggesstion-box" style="display: hide"></div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="city" class="form-control" id="city" placeholder="Enter city" value="{{ old('city') }}" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="state" class="form-control" id="state" placeholder="Enter state" value="{{ old('state') }}" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="zip" class="form-control" id="zip" placeholder="Enter zip" value="{{ old('zip') }}" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $("#suggesstion-box").hide();
                $("#street_address").keyup(function(){
                    $.ajax({
                      url: "https://us-autocomplete-pro.api.smartystreets.com/lookup?",         
                      data: {
                        "auth-id": "85858944791000110",            
                        "search": $("#street_address").val(),
                      },
                      dataType: "jsonp",
                      success: function(data) {
                        $("#suggesstion-box").html('');
                        $("#suggesstion-box").show();
                        let city='';
                        let state='';
                        let zipcode='';
                        let street_line='';
                        let data_str='';
                        if(data.suggestions)
                        {
                            for (var i = 0; i < data.suggestions.length; i++) {
                               city=data.suggestions[i].city;
                               state=data.suggestions[i].state;
                               zipcode=data.suggestions[i].zipcode;
                               street_line=data.suggestions[i].street_line;

                               data_str=street_line+'#^^#'+city+'#^^#'+state+'#^^#'+zipcode;

                               $("#suggesstion-box").append('<span onclick="get_selected(\''+data_str+'\')">'+data.suggestions[i].street_line+'</span>');
                            }
                        }
                        else
                        {
                            $("#suggesstion-box").append('<span>No data found</span>');   
                        }
                        
                      },
                      error: function(error) {
                        return error;
                      }
                    });
                });
            });

            function get_selected(data_str)
            {
              data=data_str.split("#^^#");

              let street_line = data[0];
              let city = data[1];
              let state = data[2];
              let zip = data[3];

              $("#street_address").val(street_line);
              $("#city").val(city);
              $("#state").val(state);
              $("#zip").val(zip);

              $("#suggesstion-box").html('');
              $("#suggesstion-box").hide();
            }
        </script>
    </body>
</html>