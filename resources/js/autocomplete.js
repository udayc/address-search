$(document).ready(function(){
  alert("a");
    $("#suggesstion-box").hide();
    $("#street_address").keyup(function(){
        $.ajax({
          url: "https://us-autocomplete-pro.api.smartystreets.com/lookup?",         
          data: {
            "auth-id": "85858944791000110",            
            "search": $("#street_address").val(),
          },
          dataType: "jsonp",
          success: function(data) {
            console.log("datasw",data);
            $("#suggesstion-box").html('');
            $("#suggesstion-box").show();
            let city='';
            let state='';
            let zipcode='';
            let street_line='';
            let data_str='';
            for (var i = 0; i < data.suggestions.length; i++) {
               city=data.suggestions[i].city;
               state=data.suggestions[i].state;
               zipcode=data.suggestions[i].zipcode;
               street_line=data.suggestions[i].street_line;

               data_str=street_line+'#^^#'+city+'#^^#'+state+'#^^#'+zipcode;

               $("#suggesstion-box").append('<span onclick="get_selected(\''+data_str+'\')">'+data.suggestions[i].street_line+'</span>');
            }
          },
          error: function(error) {
            return error;
          }
        });
    });
});

function get_selected(data_str)
{
  data=data_str.split("#^^#");

  let street_line = data[0];
  let city = data[1];
  let state = data[2];
  let zip = data[3];

  $("#street_address").val(street_line);
  $("#city").val(city);
  $("#state").val(state);
  $("#zip").val(zip);

  $("#suggesstion-box").html('');
  $("#suggesstion-box").hide();
}