<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;
use Redirect;
use DB;
use Mail;

class UserController extends Controller
{
	// this function is used to save all data 
    public function store(Request $request)
    {
    	// checking validation
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'phone' => 'required|string|max:100',
            'street_address' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'zip' => 'required|string|max:50',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $input=$request->all();
        $user_name=$input['name'];
        $email=$input['email'];
        $phone=$input['phone'];
        $street_address=$input['street_address'];
        $city=$input['city'];
        $state=$input['state'];
        $zip=$input['zip'];

        DB::beginTransaction();
        try {
            $user = new User();
            $user->user_name=$user_name;
            $user->email=$email;
            $user->phone=$phone;
            $user->street_address=$street_address;
            $user->city=$city;
            $user->state=$state;
            $user->zip=$zip;
            $user->status=1;
            $user->deleted=0;
            $user->save();           

            // send email
            $from_name=env('MAIL_FROM_NAME');
            $from_email=env('MAIL_FROM_ADDRESS');
            $mail_subject='Welcome to our WEBSITE';            
            $data = array('user_name' => $user_name); 

            Mail::send('email.send_mail', $data, function ($message) use ($from_email, $from_name,$email,$mail_subject) {
                $message->from($from_email, $from_name);
                $message->to($email)->subject($mail_subject);
            });
           
            DB::commit();
            return Redirect::to('/')->with('message', 'Data added successfully.');
        }catch (\Exception $e) {                
            DB::rollback(); // rollback if data not added           
            return Redirect::to('/')->with('error', 'Unable to add data with error: '.$e->getMessage());
        }
    }
}
